#!/bin/sh


# RELEASE_VERSION=1.3.0
# NEXT_VERSION=2.1.0

# cd restcountries-api-client

# git checkout main; git reset --hard f9627264d3ef689a0e32f1097b8fdb8da1c6f678; git push --force origin main
# git checkout develop; git reset --hard f9627264d3ef689a0e32f1097b8fdb8da1c6f678; git push --force origin develop
# git tag -l | xargs -n 1 git push --delete origin
# git tag | xargs git tag -d

git clean -d -f
git checkout -b release/${RELEASE_VERSION}

echo "Bumping to the next version and creating a new tag ${RELEASE_VERSION}"
npm version $RELEASE_VERSION

echo "Merge changes into main"
git checkout main
git merge release/${RELEASE_VERSION} -m "Merge release/${RELEASE_VERSION}"

echo "Merge changes into develop and bump to the next ${NEXT_VERSION}-SNAPSHOT"
git checkout develop
git merge main -m "Merge main into develop from release/${RELEASE_VERSION}"

npm version $NEXT_VERSION-SNAPSHOT --git-tag-version false
git add .
git commit -m "Ser version to  ${NEXT_VERSION}-SNAPSHOT"

echo "Push changes to the repository"
git push origin main && git push origin develop && git push origin v$RELEASE_VERSION


