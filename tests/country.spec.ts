import { RestCountriesAPI, ClientType } from '../src';

describe('RestCountriesAPI', () => {
  let restCountriesAPI: RestCountriesAPI;

  beforeAll(() => {
    restCountriesAPI = new RestCountriesAPI(ClientType.NODE);
  });

  it("Expect to return a country given it's name", async () => {
    const [country] = await restCountriesAPI.getByName('Brazil');
    expect(country.name.indexOf('Brazil') > -1).toBeTruthy();
  });

  it('Expect to return a list of countries with similar name', async () => {
    const countries = await restCountriesAPI.getByName('United');
    expect(countries.length > 1).toBeTruthy();
  });

  it('Expect to return a country when the ISO code is passed into getByISOCode()', async () => {
    const country = await restCountriesAPI.getByISOCode('col');
    expect(typeof country).toBe('object');
    expect(country.name).toBe('Colombia');
  });

  it('Expect getByISOCode() to throw an error when code parameter is not specified or set as an empty string', async () => {
    expect(restCountriesAPI.getByISOCode('')).rejects.toThrowError();
  });
});
