import { ClientType, RestCountriesAPI } from "../src";

async function main() {
  const restCountriesAPI = new RestCountriesAPI(ClientType.NODE);
  const country = await restCountriesAPI.getByName('Spain');

  console.log(country);
}

main();