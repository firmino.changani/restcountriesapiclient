import { checkParams } from './utils';
import { Country } from './entities/interface';
import { HttpClient } from './utils/http/httpClientInterface';
import { HttpClientFactory, CLIENT_TYPE } from './utils/http';

export class RestCountriesAPI {
  private httpClient!: HttpClient;

  constructor(httpClientType: CLIENT_TYPE) {
    this.httpClient = HttpClientFactory.create(httpClientType, '/rest/v2');
  }

  /**
   * Search by country name. It can be the native name or partial name
   */
  async getByName(name: string): Promise<Country[]> {
    return this.httpClient.get<Country[]>(`/name/${name}`);
  }

  /**
   * Search by ISO 3166-1 2-letter or 3-letter country code
   */
  async getByISOCode(code: string): Promise<Country> {
    checkParams([code]);

    return this.httpClient.get<Country>(`/alpha/${code}`);
  }
  // getByISOCodes(codes: string[]) {}
  // getByCurrency(currency: string) {}
  // getByLanguageCode(languageCode: string) {}
}

export default RestCountriesAPI;
