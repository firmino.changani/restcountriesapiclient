import { HttpClient } from './httpClientInterface';

export class BrowserHttpClient implements HttpClient {
  private options = {
    host: '',
  };

  constructor(baseUrl: string) {
    this.options.host = baseUrl;
  }

  async get<T>(): Promise<T> {
    console.log(this.options);
    return new Promise((resolve, reject) => {
      if (this.options) {
        resolve({} as any);
      }

      reject();
    });
  }
}

export default BrowserHttpClient;
