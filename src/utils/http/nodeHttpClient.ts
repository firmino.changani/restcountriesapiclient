import https from 'https';
import { HttpClient } from './httpClientInterface';

export class NodeHttpClient implements HttpClient {
  private basePath!: string;

  private options = {
    host: '',
    method: 'GET',
    protocol: 'https:',
  };

  constructor(host: string, basePath: string) {
    this.options.host = host;
    this.basePath = basePath;
  }

  async get<T>(url: string): Promise<T> {
    return new Promise((resolve, reject) => {
      let response = '';
      const options = {
        ...this.options,
        path: this.basePath + url,
      };
      https
        .get(options, (res) => {
          res.setEncoding('utf-8');
          res.on('data', (data) => {
            response += data;
          });

          res.on('end', () => {
            resolve(JSON.parse(response) as T);
          });
        })
        .on('error', (error) => reject(error));
    });
  }
}

export default NodeHttpClient;
