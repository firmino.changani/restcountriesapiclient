import { HttpClient } from './httpClientInterface';
import { NodeHttpClient } from './nodeHttpClient';
import { BrowserHttpClient } from './browserHttpClient';

export enum CLIENT_TYPE {
  NODE = 'NODE',
  BROWSER = 'BROWSER',
}

export class HttpClientFactory {
  public static create(type: CLIENT_TYPE, baseUrl: string): HttpClient {
    return type === CLIENT_TYPE.NODE
      ? new NodeHttpClient('restcountries.eu', baseUrl || '/rest/v2')
      : new BrowserHttpClient(baseUrl);
  }
}
